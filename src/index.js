import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter,Switch,Route,Link } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createGlobalStyle } from 'styled-components';

import store from './store';
import App from './components/App';
import Posts from './components/Posts';
import reset from './constants/css/reset';
import AddNewPost from './components/AddNewPost';

const GlobalStyle = createGlobalStyle`${reset}`;

ReactDOM.render(
    <BrowserRouter>
        <Fragment> 
            <Provider store={store}>
                {/* <App /> */}
                <Link to="/posts" >Posts  || </Link>
                <Link to="/" >Home </Link> ||
                <Link to="/add-new-post" > Add New Post </Link> ||
                <Switch>
                    <Route path="/" exact>
                        <App />
                    </Route>
                    <Route path="/posts" exact>
                        <Posts />
                    </Route>
                    <Route path="/add-new-post" exact>
                        <AddNewPost />
                    </Route>
                </Switch>
            </Provider>
            <GlobalStyle />
           
        </Fragment>

    </BrowserRouter>,
    document.getElementById('root')
);
